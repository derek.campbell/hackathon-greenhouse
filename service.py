import os
from spark import Spark
#from TemperatureDao from TemperatureDao

class Service:
    spark = ""
    dao = ""
    def __init__(self):
        self.spark = Spark(self.get_device_id(), self.get_access_token())
        #self.dao = TemperatureDao()

        #   def collect_data(self):
        #dao.insertTemperatureReading(self.get_current_temperature())

    def get_device_id(self):
        filename = os.getcwd() + '/device_id'
        f = open(filename, 'r')
        token = f.read()
        f.close()
        return token.rstrip('\n')

    def get_access_token(self):
        filename = os.getcwd() + '/access_token'
        f = open(filename, 'r')
        token = f.read()
        f.close()
        return token.rstrip('\n')

    def get_current_temperature(self):
        return self.spark.get_variable("temp")

    def get_temperature_status(self):
        return self.spark.get_variable("tempStatus")
     
    def is_daytime(self):
        return self.spark.get_variable("isDaytime")

   
if __name__ == "__main__":
    a = Service()
    print a.get_current_temperature()



