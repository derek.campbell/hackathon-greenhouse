#!/usr/bin/env python

# import twilio.twiml
import os
import time

from multiprocessing import Process
from flask import Flask, request, redirect, jsonify
from spark import Spark
from service import Service
from TemperatureDao import TemperatureDao

app = Flask(__name__)
cwd = os.path.dirname(__file__)

@app.route("/", methods=['GET', 'POST'])
def home():
    return dashboard()

@app.route("/temperature/<int:results>", methods=['GET'])
def temperatureData( results ):
    dao = TemperatureDao()
    response = dao.getTemperatureReadings( results )
    return jsonify(data=response)


@app.route("/temp", methods=['GET'])
def get_temperature():
    """Respond to the GET calls"""
    #return str(Service().get_current_temperature())
    return "Hello World!"

@app.route("/temp", methods=['POST'])
def set_temperature():
    """Respond to incoming calls with a simple text message."""
    print ""
    print "request"
    print request.args
    print ""
    print "auth"
    print request.authorization
    print "did you see it"

    return "posted!"

@app.route("/tempstatus")
def temp_status():
    status = Service().get_temperature_status()
    message = ""
    if status == -1:
        message = "cold"
    elif status == 1:
        message = "hot"
    elif status == 0:
        message = "ok"
    else:
        message = "broken"
    return message

@app.route("/isdaytime")
def is_daytime():
    daytime = Service().is_daytime()
    message = ""
    if daytime ==  0:
        message = "day"
    else:
        message = "night"
    return message


def dashboard():
    
    day = is_daytime()
    temp = get_temperature()
    temperature_status = temp_status()
    return jsonify(solar = day, temperature = temp, hot_cold =  temperature_status)

        
if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
