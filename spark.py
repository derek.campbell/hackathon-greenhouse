import sys
import requests
import json


class Spark:
    def __init__(self, deviceID, accessToken):
        self.deviceID = deviceID
        self.accessToken = accessToken


    def get_variable(self, variableName):
        r = requests.get('https://api.spark.io/v1/devices/' + self.deviceID + '/' + variableName + '?' + 'access_token=' + self.accessToken)
        #print r.url
        #print r.text
        text = r.text
        return json.loads(r.text)["result"]




if __name__ == "__main__":
    device = sys.argv[1]
    token = sys.argv[2]
    # print "device is {kwarg}".format(kwarg=device)
    # print "token is {kwarg}".format(kwarg=token)
    s = Spark(device, token)
    temp = s.get_variable("temp")
    print temp



