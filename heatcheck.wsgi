#!/usr/bin/python

import sys
import logging

logging.basicConfig(stream=sys.stderr)

sys.path.insert(0, '/home/dscamp/workspace/heatcheck/')

from heatcheck import app as application
