from TemperatureDao import TemperatureDao
from service import Service
import time

if __name__ == "__main__":
	while( 1 == 1 ) :
		dao = TemperatureDao()
		service = Service()
		
		temp = service.get_current_temperature()
		dao.insertTemperatureReading( temp )

		print "Wrote Temp: " + str( temp )
		time.sleep(5)
