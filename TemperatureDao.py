import sqlite3
from datetime import datetime

class TemperatureDao:
	dbFile = 'greenhouse.db'

	def insertTemperatureReading(self, temperature):
		conn = sqlite3.connect( self.dbFile )
		dbConn = conn.cursor()
		timestamp = datetime.now()

		dbConn.execute( 'INSERT INTO temperature (timestamp, temperature) VALUES (?,?)', (timestamp, temperature))

		conn.commit()
		conn.close()


	def getTemperatureReadings(self, numberOfRecords):
		conn = sqlite3.connect( self.dbFile )
		dbConn = conn.cursor()

		dbConn.execute('SELECT timestamp, temperature FROM temperature ORDER BY timestamp DESC LIMIT :limit', {"limit": numberOfRecords })

		result = dbConn.fetchall()
		conn.close()

		return result


if __name__ == "__main__":
	conn = sqlite3.connect( 'greenhouse.db' )
	dbConn = conn.cursor()

	dbConn.execute( 'create table if not exists temperature (timestamp text, temperature INTEGER)' )
	
	conn.commit()
	conn.close()

	app = TemperatureDao()
	print app.getTemperatureReadings(10)


